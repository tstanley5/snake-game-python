import pygame,sys,random

# write pygame.math.Vector2 as Vector2
from pygame.math import Vector2
# initialize all imported pygame modules
pygame.init()

# For moving the snake in different directions using vectors
STATIONARY = Vector2(0,0)
UP = Vector2(0,-1)
DOWN = Vector2(0,1)
RIGHT = Vector2(1,0)
LEFT = Vector2(-1,0)

clock = pygame.time.Clock()
# shows game title on the window
pygame.display.set_caption('Snake Game')

# The width and height of pygame window
cell_size = 30
cell_number = 20
screen = pygame.display.set_mode((cell_number * cell_size,cell_number * cell_size))

game_font = pygame.font.Font('snake_game_fonts/Retro Gaming.ttf',25)
main_menu_font = pygame.font.Font('snake_game_fonts/Retro Gaming.ttf', 45)
# font: Retro Gaming by Daymarius from DaFont.com

class MAIN_MENU:
    def __init__(self):
        self.main_menu_bg = pygame.image.load('snake_game_images/menu_image.PNG').convert_alpha()
    def draw_main_menu_bg(self):
        cell_product = cell_number * cell_size
        menu_image_rect = pygame.Rect(0, 0, cell_product, cell_product)
        screen.blit(self.main_menu_bg, menu_image_rect)
    def display_text(self):
        title_x_pos = int(cell_size * cell_number / 2)

        self.title_image = pygame.image.load('snake_game_images/snake_game_title_image.png').convert_alpha()
        self.title_image_rect = pygame.Rect(35, 30, 544, 148)
        screen.blit(self.title_image,self.title_image_rect)


        text_string1 = "Welcome to the Snake Game!"
        main_text1 = game_font.render(text_string1, True, "black")

        text_string2 = "Help the snake get some meals."
        main_text2 = game_font.render(text_string2, True, "black")

        text_string3 = "Use the arrow keys or WASD"
        main_text3 = game_font.render(text_string3, True, "black")

        text_string4 = "keys to control the snake."
        main_text4 = game_font.render(text_string4, True, "black")

        text_string5 = "Press the SPACE bar to play."
        main_text5 = game_font.render(text_string5, True, "black")

        main_text_x = int(cell_size * cell_number / 2)

        main_text1_rect = main_text1.get_rect(center = (main_text_x, 215))
        main_text2_rect = main_text2.get_rect(center=(main_text_x, 315))
        main_text3_rect = main_text3.get_rect(center=(main_text_x, 365))
        main_text4_rect = main_text4.get_rect(center=(main_text_x, 395))
        main_text5_rect = main_text5.get_rect(center=(main_text_x, 520))

        screen.blit(main_text1, main_text1_rect)
        screen.blit(main_text2, main_text2_rect)
        screen.blit(main_text3, main_text3_rect)
        screen.blit(main_text4, main_text4_rect)
        screen.blit(main_text5, main_text5_rect)
    def main_menu(self):
        self.main_menu_bg = pygame.image.load('snake_game_images/menu_image.PNG').convert_alpha()

        cell_product = cell_number * cell_size
        menu_image_rect = pygame.Rect(0, 0, cell_product, cell_product)
        screen.blit(self.main_menu_bg, menu_image_rect)

        On_main_menu = True
        while On_main_menu:
            screen.blit(self.main_menu_bg, menu_image_rect)
            self.display_text()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:  # pressing the X in window to close game
                    pygame.quit()  # uninitialize all pygame modules
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:  # use escape key to close game
                        pygame.quit()
                        sys.exit()
                    if event.key == pygame.K_SPACE:
                        On_main_menu = False
                        play_game()
                pygame.display.update()
                clock.tick(60)  # 60 fps
class RODENT:
    def __init__(self):  # move rodent object on screen randomly
        self.randomize()
        self.mouse = pygame.image.load("snake_game_images/mouse.png").convert_alpha()

    def draw_rodent(self):
        # creates the rectangle, int removes the implicit conversion to ints
        random_x_pos = int(self.pos.x * cell_size)
        random_y_pos = int(self.pos.y * cell_size)
        rodent_rect = pygame.Rect(random_x_pos,random_y_pos,cell_size,cell_size)
        screen.blit(self.mouse,rodent_rect)
        # draws the rectangle ( nly needed for debugging)
        #pygame.draw.rect(screen,(126,166,114),rodent_rect)

    def randomize(self):  # move rodent object on screen randomly
        self.x = random.randint(0,cell_number - 1)  # -1 so rodent stays onscreen
        self.y = random.randint(0,cell_number - 1)
        self.pos = Vector2(self.x,self.y)
class SNAKE:
    def __init__(self):  # starting position of the snake
        self.body = [Vector2(5,10),Vector2(4,10),Vector2(3,10)]
        self.direction = STATIONARY  # starts out not moving
        self.new_block = False

        # images of the snake
        self.head_up = pygame.image.load('snake_game_images/head_up.png').convert_alpha()
        self.head_down = pygame.image.load('snake_game_images/head_down.png').convert_alpha()
        self.head_right = pygame.image.load('snake_game_images/head_right.png').convert_alpha()
        self.head_left = pygame.image.load('snake_game_images/head_left.png').convert_alpha()

        self.tail_up = pygame.image.load('snake_game_images/tail_up.png').convert_alpha()
        self.tail_down = pygame.image.load('snake_game_images/tail_down.png').convert_alpha()
        self.tail_right = pygame.image.load('snake_game_images/tail_right.png').convert_alpha()
        self.tail_left = pygame.image.load('snake_game_images/tail_left.png').convert_alpha()

        self.body_vertical = pygame.image.load('snake_game_images/body_vertical.png').convert_alpha()
        self.body_horizontal = pygame.image.load('snake_game_images/body_horizontal.png').convert_alpha()

        self.body_tr = pygame.image.load('snake_game_images/body_tr.png').convert_alpha()  # top right
        self.body_tl = pygame.image.load('snake_game_images/body_tl.png').convert_alpha()  # top left
        self.body_br = pygame.image.load('snake_game_images/body_br.png').convert_alpha()  # bottom right
        self.body_bl = pygame.image.load('snake_game_images/body_bl.png').convert_alpha()  # bottom left
    def draw_snake(self):
        self.update_head_graphics()
        self.update_tail_graphics()

        for index,block in enumerate(self.body):
            # 1. We still need a rect for the positioning
            x_pos = int(block.x * cell_size)
            y_pos = int(block.y * cell_size)
            block_rect = pygame.Rect(x_pos,y_pos,cell_size,cell_size)

            # 2. What direction the face is heading
            if index == 0:
                screen.blit(self.head,block_rect)
            elif index == len(self.body) - 1:
                screen.blit(self.tail,block_rect)
            else:
                previous_block = self.body[index + 1] - block
                next_block = self.body[index - 1] - block
                if previous_block.x == next_block.x:
                    screen.blit(self.body_vertical,block_rect)
                elif previous_block.y == next_block.y:
                    screen.blit(self.body_horizontal,block_rect)
                else:
                    if previous_block.x == -1 and next_block.y == -1 or previous_block.y == -1 and next_block.x == -1:
                        screen.blit(self.body_tl,block_rect)
                    elif previous_block.x == -1 and next_block.y == 1 or previous_block.y == 1 and next_block.x == -1:
                        screen.blit(self.body_bl,block_rect)
                    elif previous_block.x == 1 and next_block.y == -1 or previous_block.y == -1 and next_block.x == 1:
                        screen.blit(self.body_tr,block_rect)
                    elif previous_block.x == 1 and next_block.y == 1 or previous_block.y == 1 and next_block.x == 1:
                        screen.blit(self.body_br,block_rect)

    def update_head_graphics(self):
        head_relation = self.body[1] - self.body[0]
        if head_relation == RIGHT:
            self.head = self.head_left
        elif head_relation == LEFT:
            self.head = self.head_right
        elif head_relation == DOWN:
            self.head = self.head_up
        elif head_relation == UP:
            self.head = self.head_down

    def update_tail_graphics(self):
        tail_relation = self.body[-2] - self.body[-1]
        if tail_relation == RIGHT:
            self.tail = self.tail_left
        elif tail_relation == LEFT:
            self.tail = self.tail_right
        elif tail_relation == DOWN:
            self.tail = self.tail_up
        elif tail_relation == UP:
            self.tail = self.tail_down

    def move_snake(self):
        if self.new_block == True:
            body_copy = self.body[:]  # uses the first two elements of self.body
            body_copy.insert(0, body_copy[0] + self.direction)
            self.body = body_copy[:]
            self.new_block = False

        else:  # move snake without changing length
            body_copy = self.body[:-1]  # uses the first two elements of self.body
            body_copy.insert(0,body_copy[0] + self.direction)
            self.body = body_copy[:]

    def add_block(self):
        self.new_block = True

    def reset(self):
        self.body=[Vector2(5,10), Vector2(4,10), Vector2(3,10)]
        self.direction = STATIONARY # snake is stationary
class BACKGROUND:
    def __init__(self):
        self.background = pygame.image.load('snake_game_images/background.png').convert_alpha()
    def draw_background(self):
        cell_product = cell_number * cell_size
        background_rect = pygame.Rect(0,0,cell_product,cell_product)
        screen.blit(self.background,background_rect)
class MAIN:
    def __init__(self):
        self.background = BACKGROUND() # create the background object, the playing field
        self.snake = SNAKE()    # create the snake object
        self.rodent = RODENT()  # create the rodent object
    def update(self):
        self.snake.move_snake()
        self.check_collision()
        self.check_fail()
    def draw_elements(self):
        self.background.draw_background()
        self.rodent.draw_rodent()
        self.snake.draw_snake()
        self.draw_score()
    def check_collision(self):
        # if the rodent's pos is the same as head of snake
        if self.rodent.pos == self.snake.body[0]:  # head of snake
            self.rodent.randomize()  # reposition the rodent somewhere else
            self.snake.add_block()

        for block in self.snake.body[1:]: # rerandomize pos if the rodent is at the snake's body or head
            if block == self.rodent.pos:
                self.rodent.randomize()
    def check_fail(self): # if snake head's pos is not between 0 and cell num
        if not 0 <= self.snake.body[0].x < cell_number or not 0 <= self.snake.body[0].y < cell_number:
            self.game_over()
        for block in self.snake.body[1:]:
            if block == self.snake.body[0]:
                self.game_over()
    def game_over(self):
        self.snake.reset()
    def draw_score(self):
        score_text = str(len(self.snake.body) - 3)
        score_surface = game_font.render(score_text,True,(0,0,0))
        score_x = int(cell_size * cell_number / 2)
        score_y = int(cell_size * cell_number - 580)
        score_rect = score_surface.get_rect(center = (score_x, score_y))
        new_rodent_rect = score_surface.get_rect(midright = (score_rect.left - 15, score_rect.centery))
        bg_rect = pygame.Rect(new_rodent_rect.left, new_rodent_rect.top, new_rodent_rect.width + score_rect.width + 20, new_rodent_rect.height)

        pygame.draw.rect(screen,(255,245,83), bg_rect)
        screen.blit(score_surface, score_rect)
        screen.blit(main_game.rodent.mouse, new_rodent_rect)
        pygame.draw.rect(screen,(0,0,0), bg_rect,2) # frame of the score

SCREEN_UPDATE = pygame.USEREVENT
# SCREEN_UPDATE happens every 150 milliseconds
pygame.time.set_timer(SCREEN_UPDATE,150)

main_game = MAIN()
main_menu = MAIN_MENU()

def play_game():
    Game_is_Running = True
    while Game_is_Running:
        # To make sure the snake doesn't go the opposite direction
        # of its prior direction
        changed_direction = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:  # pressing the X in window to close game
                pygame.quit()  # uninitialize all pygame modules
                sys.exit()
            if event.type == SCREEN_UPDATE:
                main_game.update()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:  # use escape key to close game
                    pygame.quit()
                    sys.exit()
                if changed_direction == False:
                    if event.key == pygame.K_UP and main_game.snake.direction != DOWN:
                        main_game.snake.direction = UP
                        changed_direction = True
                    elif event.key == pygame.K_w and main_game.snake.direction != DOWN:
                        main_game.snake.direction = UP
                        changed_direction = True
                    elif event.key == pygame.K_DOWN and main_game.snake.direction != UP:
                        main_game.snake.direction = DOWN
                        changed_direction = True
                    elif event.key == pygame.K_s and main_game.snake.direction != UP:
                        main_game.snake.direction = DOWN
                        changed_direction = True
                    elif event.key == pygame.K_RIGHT and main_game.snake.direction != LEFT:
                        main_game.snake.direction = RIGHT
                        changed_direction = True
                    elif event.key == pygame.K_d and main_game.snake.direction != LEFT:
                        main_game.snake.direction = RIGHT
                        changed_direction = True
                    elif event.key == pygame.K_LEFT and main_game.snake.direction != RIGHT:
                        main_game.snake.direction = LEFT
                        changed_direction = True
                    elif event.key == pygame.K_a and main_game.snake.direction != RIGHT:
                        main_game.snake.direction = LEFT
                        changed_direction = True
            screen.fill((175,215,150))  # the display surface color
            main_game.draw_elements()
            pygame.display.update()
            clock.tick(60)  # 60 fps

main_menu.main_menu()